<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<TcPlcObject ProductVersion="3.1.0.18" Version="1.1.0.1">
  <POU Name="MAIN">
    <Declaration><![CDATA[PROGRAM MAIN
VAR
    timer_t: LREAL;
    preset_timer_t: TIME;
    timer: timer_location;
    sup: sup_location;
    hw_button_bit: BOOL;
    hw_button: hw_button_location;
    hw_lamp_bit: BOOL;
    hw_lamp: hw_lamp_location;
    firstRun: BOOL := TRUE;
END_VAR
VAR_TEMP
    curValue: LREAL;
    current_hw_button: hw_button_location;
    current_hw_button_1: hw_button_location;
    current_hw_lamp: hw_lamp_location;
    current_hw_lamp_1: hw_lamp_location;
    current_hw_lamp_bit: BOOL;
    current_hw_lamp_bit_1: BOOL;
    current_sup: sup_location;
    current_sup_1: sup_location;
    current_sup_2: sup_location;
    current_sup_3: sup_location;
    current_sup_4: sup_location;
    current_sup_5: sup_location;
    current_timer: timer_location;
    current_timer_1: timer_location;
    current_timer_t: LREAL;
    edge_hw_button_1: DINT;
    edge_hw_lamp_1: DINT;
    edge_sup_1: DINT;
    edge_timer_1: DINT;
    eventEnabled: BOOL;
    isProgress: BOOL;
    timeOut: BOOL;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[(* --- Read input from sensors. -------------------------------------------- *)
hw_button_bit := in_hw_button_bit;

(* --- Initialize state or update continuous variables. -------------------- *)
IF firstRun THEN
    firstRun := FALSE;

    (* Initialize the state variables. *)
    timer_t := 0.0;
    (* Reset timer of "timer_t". *)
    preset_timer_t := timer_t;
    ton_timer_t(IN := FALSE, PT := preset_timer_t);
    ton_timer_t(IN := TRUE, PT := preset_timer_t);
    timer := timer_Idle;
    sup := sup_s1;
    hw_button := hw_button_Released;
    hw_lamp_bit := FALSE;
    hw_lamp := hw_lamp_Off;
ELSE
    (* Update remaining time of "timer_t". *)
    ton_timer_t(IN := TRUE, PT := preset_timer_t, Q => timeOut, ET => curValue);
    timer_t := SEL(timeOut, preset_timer_t - curValue, 0.0);
END_IF;

(* --- Process all events. ------------------------------------------------- *)
isProgress := TRUE;
(* Perform events until none can be done anymore. *)
WHILE isProgress DO
    isProgress := FALSE;

    (*************************************************************
     * Try to perform event "timer.start".
     *
     * - Automaton "timer" must always synchronize.
     * - Automaton "sup" must always synchronize.
     *************************************************************)
    eventEnabled := TRUE;
    (*******************************
     * Check each synchronizing automaton for having an edge with a true guard.
     *******************************)
    (***********
     * Testing edge of automaton "timer" to synchronize for event "timer.start".
     * This automaton must have an edge with a true guard to allow the event.
     *
     * Edge being tested:
     * - Location "Idle":
     *   - 1st edge in the location
     ***********)
    IF timer = timer_Idle THEN
        edge_timer_1 := 1;
    ELSE
        (* The automaton has no edge with a true guard. Skip to the next event. *)
        eventEnabled := FALSE;
    END_IF;
    IF eventEnabled THEN
        (***********
         * Testing edges of automaton "sup" to synchronize for event "timer.start".
         * This automaton must have an edge with a true guard to allow the event.
         *
         * Edges being tested:
         * - Location "s3":
         *   - 1st edge in the location
         * - Location "s5":
         *   - 1st edge in the location
         ***********)
        IF sup = sup_s3 THEN
            edge_sup_1 := 1;
        ELSIF sup = sup_s5 THEN
            edge_sup_1 := 2;
        ELSE
            (* The automaton has no edge with a true guard. Skip to the next event. *)
            eventEnabled := FALSE;
        END_IF;
    END_IF;
    (* All checks have been done. If variable "eventEnabled" still holds, event "timer.start" can occur. *)
    IF eventEnabled THEN
        isProgress := TRUE;
        (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
        current_sup := sup;
        current_timer := timer;
        current_timer_t := timer_t;
        (*******************************
         * Perform the assignments of each synchronizing automaton.
         *******************************)
        (* Perform assignments of automaton "timer". *)
        IF edge_timer_1 = 1 THEN
            (* Perform assignments of the 1st edge in location "timer.Idle". *)
            timer_t := 3.0;
            (* Reset timer of "timer_t". *)
            preset_timer_t := timer_t;
            ton_timer_t(IN := FALSE, PT := preset_timer_t);
            ton_timer_t(IN := TRUE, PT := preset_timer_t);
            timer := timer_Running;
        END_IF;
        (* Perform assignments of automaton "sup". *)
        IF edge_sup_1 = 1 THEN
            (* Perform assignments of the 1st edge in location "sup.s3". *)
            sup := sup_s6;
        ELSIF edge_sup_1 = 2 THEN
            (* Perform assignments of the 1st edge in location "sup.s5". *)
            sup := sup_s7;
        END_IF;
    END_IF;

    (*************************************************************
     * Try to perform event "timer.timeout".
     *
     * - Automaton "timer" must always synchronize.
     * - Automaton "sup" must always synchronize.
     *************************************************************)
    eventEnabled := TRUE;
    (*******************************
     * Check each synchronizing automaton for having an edge with a true guard.
     *******************************)
    (***********
     * Testing edge of automaton "timer" to synchronize for event "timer.timeout".
     * This automaton must have an edge with a true guard to allow the event.
     *
     * Edge being tested:
     * - Location "Running":
     *   - 1st edge in the location
     ***********)
    IF timer = timer_Running AND timer_t <= 0.0 THEN
        edge_timer_1 := 1;
    ELSE
        (* The automaton has no edge with a true guard. Skip to the next event. *)
        eventEnabled := FALSE;
    END_IF;
    IF eventEnabled THEN
        (***********
         * Testing edges of automaton "sup" to synchronize for event "timer.timeout".
         * This automaton must have an edge with a true guard to allow the event.
         *
         * Edges being tested:
         * - Location "s6":
         *   - 1st edge in the location
         * - Location "s7":
         *   - 1st edge in the location
         ***********)
        IF sup = sup_s6 THEN
            edge_sup_1 := 1;
        ELSIF sup = sup_s7 THEN
            edge_sup_1 := 2;
        ELSE
            (* The automaton has no edge with a true guard. Skip to the next event. *)
            eventEnabled := FALSE;
        END_IF;
    END_IF;
    (* All checks have been done. If variable "eventEnabled" still holds, event "timer.timeout" can occur. *)
    IF eventEnabled THEN
        isProgress := TRUE;
        (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
        current_sup_1 := sup;
        current_timer_1 := timer;
        (*******************************
         * Perform the assignments of each synchronizing automaton.
         *******************************)
        (* Perform assignments of automaton "timer". *)
        IF edge_timer_1 = 1 THEN
            (* Perform assignments of the 1st edge in location "timer.Running". *)
            timer := timer_Idle;
        END_IF;
        (* Perform assignments of automaton "sup". *)
        IF edge_sup_1 = 1 THEN
            (* Perform assignments of the 1st edge in location "sup.s6". *)
            sup := sup_s8;
        ELSIF edge_sup_1 = 2 THEN
            (* Perform assignments of the 1st edge in location "sup.s7". *)
            sup := sup_s9;
        END_IF;
    END_IF;

    (*************************************************************
     * Try to perform event "button.push".
     *
     * - Automaton "sup" must always synchronize.
     * - Automaton "hw_button" must always synchronize.
     *************************************************************)
    eventEnabled := TRUE;
    (*******************************
     * Check each synchronizing automaton for having an edge with a true guard.
     *******************************)
    (***********
     * Testing edges of automaton "sup" to synchronize for event "button.push".
     * This automaton must have an edge with a true guard to allow the event.
     *
     * Edges being tested:
     * - Location "s1":
     *   - 1st edge in the location
     * - Location "s4":
     *   - 2nd edge in the location
     * - Location "s5":
     *   - 2nd edge in the location
     * - Location "s7":
     *   - 2nd edge in the location
     * - Location "s9":
     *   - 2nd edge in the location
     ***********)
    IF sup = sup_s1 THEN
        edge_sup_1 := 1;
    ELSIF sup = sup_s4 THEN
        edge_sup_1 := 2;
    ELSIF sup = sup_s5 THEN
        edge_sup_1 := 3;
    ELSIF sup = sup_s7 THEN
        edge_sup_1 := 4;
    ELSIF sup = sup_s9 THEN
        edge_sup_1 := 5;
    ELSE
        (* The automaton has no edge with a true guard. Skip to the next event. *)
        eventEnabled := FALSE;
    END_IF;
    IF eventEnabled THEN
        (***********
         * Testing edge of automaton "hw_button" to synchronize for event "button.push".
         * This automaton must have an edge with a true guard to allow the event.
         *
         * Edge being tested:
         * - Location "Released":
         *   - 1st edge in the location
         ***********)
        IF hw_button = hw_button_Released AND hw_button_bit THEN
            edge_hw_button_1 := 1;
        ELSE
            (* The automaton has no edge with a true guard. Skip to the next event. *)
            eventEnabled := FALSE;
        END_IF;
    END_IF;
    (* All checks have been done. If variable "eventEnabled" still holds, event "button.push" can occur. *)
    IF eventEnabled THEN
        isProgress := TRUE;
        (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
        current_hw_button := hw_button;
        current_sup_2 := sup;
        (*******************************
         * Perform the assignments of each synchronizing automaton.
         *******************************)
        (* Perform assignments of automaton "sup". *)
        IF edge_sup_1 = 1 THEN
            (* Perform assignments of the 1st edge in location "sup.s1". *)
            sup := sup_s2;
        ELSIF edge_sup_1 = 2 THEN
            (* Perform assignments of the 2nd edge in location "sup.s4". *)
            sup := sup_s2;
        ELSIF edge_sup_1 = 3 THEN
            (* Perform assignments of the 2nd edge in location "sup.s5". *)
            sup := sup_s3;
        ELSIF edge_sup_1 = 4 THEN
            (* Perform assignments of the 2nd edge in location "sup.s7". *)
            sup := sup_s6;
        ELSIF edge_sup_1 = 5 THEN
            (* Perform assignments of the 2nd edge in location "sup.s9". *)
            sup := sup_s8;
        END_IF;
        (* Perform assignments of automaton "hw_button". *)
        IF edge_hw_button_1 = 1 THEN
            (* Perform assignments of the 1st edge in location "hw_button.Released". *)
            hw_button := hw_button_Pushed;
        END_IF;
    END_IF;

    (*************************************************************
     * Try to perform event "button.release".
     *
     * - Automaton "sup" must always synchronize.
     * - Automaton "hw_button" must always synchronize.
     *************************************************************)
    eventEnabled := TRUE;
    (*******************************
     * Check each synchronizing automaton for having an edge with a true guard.
     *******************************)
    (***********
     * Testing edges of automaton "sup" to synchronize for event "button.release".
     * This automaton must have an edge with a true guard to allow the event.
     *
     * Edges being tested:
     * - Location "s2":
     *   - 2nd edge in the location
     * - Location "s3":
     *   - 2nd edge in the location
     * - Location "s6":
     *   - 2nd edge in the location
     * - Location "s8":
     *   - 2nd edge in the location
     * - Location "s10":
     *   - 1st edge in the location
     ***********)
    IF sup = sup_s2 THEN
        edge_sup_1 := 1;
    ELSIF sup = sup_s3 THEN
        edge_sup_1 := 2;
    ELSIF sup = sup_s6 THEN
        edge_sup_1 := 3;
    ELSIF sup = sup_s8 THEN
        edge_sup_1 := 4;
    ELSIF sup = sup_s10 THEN
        edge_sup_1 := 5;
    ELSE
        (* The automaton has no edge with a true guard. Skip to the next event. *)
        eventEnabled := FALSE;
    END_IF;
    IF eventEnabled THEN
        (***********
         * Testing edge of automaton "hw_button" to synchronize for event "button.release".
         * This automaton must have an edge with a true guard to allow the event.
         *
         * Edge being tested:
         * - Location "Pushed":
         *   - 1st edge in the location
         ***********)
        IF hw_button = hw_button_Pushed AND NOT hw_button_bit THEN
            edge_hw_button_1 := 1;
        ELSE
            (* The automaton has no edge with a true guard. Skip to the next event. *)
            eventEnabled := FALSE;
        END_IF;
    END_IF;
    (* All checks have been done. If variable "eventEnabled" still holds, event "button.release" can occur. *)
    IF eventEnabled THEN
        isProgress := TRUE;
        (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
        current_hw_button_1 := hw_button;
        current_sup_3 := sup;
        (*******************************
         * Perform the assignments of each synchronizing automaton.
         *******************************)
        (* Perform assignments of automaton "sup". *)
        IF edge_sup_1 = 1 THEN
            (* Perform assignments of the 2nd edge in location "sup.s2". *)
            sup := sup_s4;
        ELSIF edge_sup_1 = 2 THEN
            (* Perform assignments of the 2nd edge in location "sup.s3". *)
            sup := sup_s5;
        ELSIF edge_sup_1 = 3 THEN
            (* Perform assignments of the 2nd edge in location "sup.s6". *)
            sup := sup_s7;
        ELSIF edge_sup_1 = 4 THEN
            (* Perform assignments of the 2nd edge in location "sup.s8". *)
            sup := sup_s9;
        ELSIF edge_sup_1 = 5 THEN
            (* Perform assignments of the 1st edge in location "sup.s10". *)
            sup := sup_s1;
        END_IF;
        (* Perform assignments of automaton "hw_button". *)
        IF edge_hw_button_1 = 1 THEN
            (* Perform assignments of the 1st edge in location "hw_button.Pushed". *)
            hw_button := hw_button_Released;
        END_IF;
    END_IF;

    (*************************************************************
     * Try to perform event "lamp.on".
     *
     * - Automaton "sup" must always synchronize.
     * - Automaton "hw_lamp" must always synchronize.
     *************************************************************)
    eventEnabled := TRUE;
    (*******************************
     * Check each synchronizing automaton for having an edge with a true guard.
     *******************************)
    (***********
     * Testing edges of automaton "sup" to synchronize for event "lamp.on".
     * This automaton must have an edge with a true guard to allow the event.
     *
     * Edges being tested:
     * - Location "s2":
     *   - 1st edge in the location
     * - Location "s4":
     *   - 1st edge in the location
     ***********)
    IF sup = sup_s2 THEN
        edge_sup_1 := 1;
    ELSIF sup = sup_s4 THEN
        edge_sup_1 := 2;
    ELSE
        (* The automaton has no edge with a true guard. Skip to the next event. *)
        eventEnabled := FALSE;
    END_IF;
    IF eventEnabled THEN
        (***********
         * Testing edge of automaton "hw_lamp" to synchronize for event "lamp.on".
         * This automaton must have an edge with a true guard to allow the event.
         *
         * Edge being tested:
         * - Location "Off":
         *   - 1st edge in the location
         ***********)
        IF hw_lamp = hw_lamp_Off THEN
            edge_hw_lamp_1 := 1;
        ELSE
            (* The automaton has no edge with a true guard. Skip to the next event. *)
            eventEnabled := FALSE;
        END_IF;
    END_IF;
    (* All checks have been done. If variable "eventEnabled" still holds, event "lamp.on" can occur. *)
    IF eventEnabled THEN
        isProgress := TRUE;
        (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
        current_hw_lamp := hw_lamp;
        current_hw_lamp_bit := hw_lamp_bit;
        current_sup_4 := sup;
        (*******************************
         * Perform the assignments of each synchronizing automaton.
         *******************************)
        (* Perform assignments of automaton "sup". *)
        IF edge_sup_1 = 1 THEN
            (* Perform assignments of the 1st edge in location "sup.s2". *)
            sup := sup_s3;
        ELSIF edge_sup_1 = 2 THEN
            (* Perform assignments of the 1st edge in location "sup.s4". *)
            sup := sup_s5;
        END_IF;
        (* Perform assignments of automaton "hw_lamp". *)
        IF edge_hw_lamp_1 = 1 THEN
            (* Perform assignments of the 1st edge in location "hw_lamp.Off". *)
            hw_lamp_bit := TRUE;
            hw_lamp := hw_lamp_On;
        END_IF;
    END_IF;

    (*************************************************************
     * Try to perform event "lamp.off".
     *
     * - Automaton "sup" must always synchronize.
     * - Automaton "hw_lamp" must always synchronize.
     *************************************************************)
    eventEnabled := TRUE;
    (*******************************
     * Check each synchronizing automaton for having an edge with a true guard.
     *******************************)
    (***********
     * Testing edges of automaton "sup" to synchronize for event "lamp.off".
     * This automaton must have an edge with a true guard to allow the event.
     *
     * Edges being tested:
     * - Location "s8":
     *   - 1st edge in the location
     * - Location "s9":
     *   - 1st edge in the location
     ***********)
    IF sup = sup_s8 THEN
        edge_sup_1 := 1;
    ELSIF sup = sup_s9 THEN
        edge_sup_1 := 2;
    ELSE
        (* The automaton has no edge with a true guard. Skip to the next event. *)
        eventEnabled := FALSE;
    END_IF;
    IF eventEnabled THEN
        (***********
         * Testing edge of automaton "hw_lamp" to synchronize for event "lamp.off".
         * This automaton must have an edge with a true guard to allow the event.
         *
         * Edge being tested:
         * - Location "On":
         *   - 1st edge in the location
         ***********)
        IF hw_lamp = hw_lamp_On THEN
            edge_hw_lamp_1 := 1;
        ELSE
            (* The automaton has no edge with a true guard. Skip to the next event. *)
            eventEnabled := FALSE;
        END_IF;
    END_IF;
    (* All checks have been done. If variable "eventEnabled" still holds, event "lamp.off" can occur. *)
    IF eventEnabled THEN
        isProgress := TRUE;
        (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
        current_hw_lamp_1 := hw_lamp;
        current_hw_lamp_bit_1 := hw_lamp_bit;
        current_sup_5 := sup;
        (*******************************
         * Perform the assignments of each synchronizing automaton.
         *******************************)
        (* Perform assignments of automaton "sup". *)
        IF edge_sup_1 = 1 THEN
            (* Perform assignments of the 1st edge in location "sup.s8". *)
            sup := sup_s10;
        ELSIF edge_sup_1 = 2 THEN
            (* Perform assignments of the 1st edge in location "sup.s9". *)
            sup := sup_s1;
        END_IF;
        (* Perform assignments of automaton "hw_lamp". *)
        IF edge_hw_lamp_1 = 1 THEN
            (* Perform assignments of the 1st edge in location "hw_lamp.On". *)
            hw_lamp_bit := FALSE;
            hw_lamp := hw_lamp_Off;
        END_IF;
    END_IF;
END_WHILE;

(* --- Write output to actuators. ------------------------------------------ *)
out_hw_lamp_bit := hw_lamp_bit;]]></ST>
    </Implementation>
    <ObjectProperties/>
  </POU>
</TcPlcObject>
