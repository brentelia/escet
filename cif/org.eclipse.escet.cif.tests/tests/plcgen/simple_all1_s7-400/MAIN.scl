ORGANIZATION_BLOCK MAIN
{ S7_Optimized_Access := 'false' }
    VAR_TEMP
        curValue: REAL;
        current_p: DINT;
        current_p_b: BOOL;
        current_p_j: DINT;
        current_p_r: REAL;
        current_p_tv: TupleStruct_2;
        current_p_v3: TupleStruct_1;
        current_p_ve: DINT;
        current_p_x: DINT;
        edge_p_1: DINT;
        eventEnabled: BOOL;
        isProgress: BOOL;
        litStruct: TupleStruct;
        litStruct_1: TupleStruct_1;
        litStruct_2: TupleStruct_2;
        rightValue: TupleStruct_2;
        timeOut: BOOL;
    END_VAR

BEGIN
    (* --- Initialize state or update continuous variables. -------------------- *)
    IF "DB".firstRun THEN
        "DB".firstRun := FALSE;

        (* Initialize the state variables. *)
        "DB".p_b := TRUE;
        "DB".p_x := 0;
        "DB".p_y := 1.23;
        "DB".p_ve := 0;
        "DB".p_v1 := 0;
        litStruct.field1 := 0.0;
        litStruct.field2 := 0.0;
        "DB".p_v2 := litStruct;
        litStruct.field1 := 0.0;
        litStruct.field2 := 0.0;
        litStruct_1.field1 := litStruct;
        litStruct_1.field2 := 0.0;
        "DB".p_v3 := litStruct_1;
        litStruct_2.field1 := 0;
        litStruct_2.field2 := 0;
        "DB".p_tv := litStruct_2;
        "DB".p_j := 0;
        "DB".p_r := 1000000.0;
        "DB".p_t := 0.0;
        (* Reset timer of "p_t". *)
        "DB".preset_p_t := "DB".p_t;
        ton_p_t.TON(IN := FALSE, PT := "DB".preset_p_t);
        ton_p_t.TON(IN := TRUE, PT := "DB".preset_p_t);
        "DB".p := 0;
    ELSE
        (* Update remaining time of "p_t". *)
        ton_p_t.TON(IN := TRUE, PT := "DB".preset_p_t, Q => timeOut, ET => curValue);
        "DB".p_t := SEL(timeOut, "DB".preset_p_t - curValue, 0.0);
    END_IF;

    (* --- Process all events. ------------------------------------------------- *)
    isProgress := TRUE;
    (* Perform events until none can be done anymore. *)
    WHILE isProgress DO
        isProgress := FALSE;

        (*************************************************************
         * Try to perform event "p.evt".
         *
         * - Automaton "p" must always synchronize.
         *************************************************************)
        eventEnabled := TRUE;
        (*******************************
         * Check each synchronizing automaton for having an edge with a true guard.
         *******************************)
        (***********
         * Testing edges of automaton "p" to synchronize for event "p.evt".
         * This automaton must have an edge with a true guard to allow the event.
         *
         * Edges being tested:
         * - Location "l1":
         *   - 1st edge in the location
         * - Location "l6":
         *   - 1st edge in the location
         * - Location "l7":
         *   - 1st edge in the location
         * - Location "l8":
         *   - 1st edge in the location
         * - Location "l9":
         *   - 1st edge in the location
         * - Location "l10":
         *   - 1st edge in the location
         * - Location "l11":
         *   - 1st edge in the location
         * - Location "l12":
         *   - 1st edge in the location
         * - Location "l13":
         *   - 1st edge in the location
         * - Location "l14":
         *   - 1st edge in the location
         * - Location "l15":
         *   - 1st edge in the location
         ***********)
        IF "DB".p = 0 THEN
            edge_p_1 := 1;
        ELSIF "DB".p = 1 THEN
            edge_p_1 := 2;
        ELSIF "DB".p = 2 THEN
            edge_p_1 := 3;
        ELSIF "DB".p = 3 THEN
            edge_p_1 := 4;
        ELSIF "DB".p = 4 THEN
            edge_p_1 := 5;
        ELSIF "DB".p = 5 THEN
            edge_p_1 := 6;
        ELSIF "DB".p = 6 THEN
            edge_p_1 := 7;
        ELSIF "DB".p = 7 THEN
            edge_p_1 := 8;
        ELSIF "DB".p = 8 THEN
            edge_p_1 := 9;
        ELSIF "DB".p = 9 THEN
            edge_p_1 := 10;
        ELSIF "DB".p = 10 THEN
            edge_p_1 := 11;
        ELSE
            (* The automaton has no edge with a true guard. Skip to the next event. *)
            eventEnabled := FALSE;
        END_IF;
        (* All checks have been done. If variable "eventEnabled" still holds, event "p.evt" can occur. *)
        IF eventEnabled THEN
            isProgress := TRUE;
            (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
            current_p := "DB".p;
            current_p_b := "DB".p_b;
            current_p_j := "DB".p_j;
            current_p_r := "DB".p_r;
            current_p_tv := "DB".p_tv;
            current_p_v3 := "DB".p_v3;
            current_p_ve := "DB".p_ve;
            current_p_x := "DB".p_x;
            (*******************************
             * Perform the assignments of each synchronizing automaton.
             *******************************)
            (* Perform assignments of automaton "p". *)
            IF edge_p_1 = 1 THEN
                (* Perform assignments of the 1st edge in location "p.l1". *)
                litStruct_2.field1 := 1;
                litStruct_2.field2 := 2;
                "DB".p_tv := litStruct_2;
                "DB".p := 1;
            ELSIF edge_p_1 = 2 THEN
                (* Perform assignments of the 1st edge in location "p.l6". *)
                "DB".p_x := 3;
                "DB".p_j := 4;
                "DB".p := 2;
            ELSIF edge_p_1 = 3 THEN
                (* Perform assignments of the 1st edge in location "p.l7". *)
                rightValue := current_p_tv;
                "DB".p_x := rightValue.field1;
                "DB".p_j := rightValue.field2;
                "DB".p := 3;
            ELSIF edge_p_1 = 4 THEN
                (* Perform assignments of the 1st edge in location "p.l8". *)
                "DB".p_tv.field1 := 5;
                "DB".p := 4;
            ELSIF edge_p_1 = 5 THEN
                (* Perform assignments of the 1st edge in location "p.l9". *)
                "DB".p_tv.field2 := 6;
                "DB".p_j := 7;
                "DB".p := 5;
            ELSIF edge_p_1 = 6 THEN
                (* Perform assignments of the 1st edge in location "p.l10". *)
                "DB".p_v3.field1.field2 := 7.8;
                "DB".p := 6;
            ELSIF edge_p_1 = 7 THEN
                (* Perform assignments of the 1st edge in location "p.l11". *)
                litStruct.field1 := 1.2;
                litStruct.field2 := 3.4;
                litStruct_1.field1 := litStruct;
                litStruct_1.field2 := 5.6;
                "DB".p_v3 := litStruct_1;
                "DB".p := 7;
            ELSIF edge_p_1 = 8 THEN
                (* Perform assignments of the 1st edge in location "p.l12". *)
                "DB".p_j := current_p_j + 1;
                "DB".p := 8;
            ELSIF edge_p_1 = 9 THEN
                (* Perform assignments of the 1st edge in location "p.l13". *)
                "DB".p_b := FALSE;
                "DB".p := 9;
            ELSIF edge_p_1 = 10 THEN
                (* Perform assignments of the 1st edge in location "p.l14". *)
                IF NOT current_p_b THEN
                    "DB".p_ve := 1;
                END_IF;
                "DB".p := 10;
            ELSIF edge_p_1 = 11 THEN
                (* Perform assignments of the 1st edge in location "p.l15". *)
                "DB".p_r := DINT_TO_REAL("DB".g_inp) + 10.0;
                "DB".p := 11;
            END_IF;
        END_IF;
    END_WHILE;
END_ORGANIZATION_BLOCK
